namespace :players do
  desc "Set player's salary to 0 if nil"
  task set_players_salary_to_zero_if_nil: :environment do
    Player.where(salary: nil).each do |player|
      player.update!(salary: 0)
    end
  end
end
