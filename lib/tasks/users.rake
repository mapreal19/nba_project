namespace :users do
  desc "Drop user players if budget is illegal"
  task drop_user_players: :environment do
    User.find_each do |user|
      if user.budget_spent > UserPlayer::BUDGET
        user.user_players.delete_all
      end
    end
  end
end
