namespace :nba do
  desc "Gets the Game Logs from Nba.com and stores them in the database"
  task game_logs: :environment do
    NbaGameLogs.run
  end
end
