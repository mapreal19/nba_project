namespace :encestado do
  desc "When called creates a week in Encestado from now to 1 week after"
  task create_week: :environment do
    AppCreateWeek.run(Date.current)
  end
end