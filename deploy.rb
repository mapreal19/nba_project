#!/usr/bin/env ruby

def check_if_master()
  puts 'Checking you are in branch master...'
  branch_name = `git rev-parse --abbrev-ref HEAD`.chomp
  if branch_name != 'master'
    abort 'You need to be in master branch in order to deploy'
  end
end

def run_specs()
  puts 'Running specs...'
  out = `rspec`
  out.each_line do |line|
    if line =~ /example.*failure/
      num_failures = /(?<num_failures>\d) failure/.match(line)[:num_failures]
      if num_failures.to_i != 0
        abort 'Specs have errors'
      end
      puts line
    end
  end  
  puts 'Specs passed! :D'
end

def precompile_assets()
  puts 'precompiling assets...'
  `rake assets:precompile RAILS_ENV=production`
  `git add .`
  `git commit -m 'Precompiled assets'`
end

def push_to_dokku()
  `git push dokku master`
  `ssh -t dokku@encestado.com run encestado.com rake db:migrate`
  puts 'App deployed :)'
end

# MAIN

check_if_master()
run_specs()
precompile_assets()
push_to_dokku()