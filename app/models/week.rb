class Week < ActiveRecord::Base

  has_many :ef_points, dependent: :destroy
  has_many :user_points, dependent: :destroy
  has_many :user_lineups, dependent: :destroy

  def self.last_week_id
    week = Week.last
    week.nil? ? '1' : week.id
  end

  def week_with_id
    It.it('ranking.week_with_id', id: id)
  end

  # gets the week that was before the instance
  def previous_week
    self.class.where("id < ?", self.id).order("id DESC").first
  end
end
