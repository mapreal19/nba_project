class Player < ActiveRecord::Base
  extend FriendlyId
  friendly_id :display_first_last, use: :slugged
  
  has_many :game_logs
  has_many :ef_points
  belongs_to :team
  
  has_attached_file :avatar, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  
  scope :with_salary, -> { where('salary > ?', 0) }
  scope :salary_desc, -> { order('salary DESC') }
  
  scope :average_encestado_points_between, ->(start_date, end_date) do 
    select('AVG(encestado_points) AS average, players.id').
      joins(:game_logs).
      where('game_logs.game_date' => start_date..end_date).
      group('players.id') + Player.with_no_games_between(start_date, end_date)
  end
  
  scope :with_no_games_between, ->(start_date, end_date) do
    select('id, 0 as average').where.not(
        id: GameLog.select('player_id').
          where('game_logs.game_date' => start_date..end_date)
        )
  end

  def last_five_games
    game_logs
      .order('game_date DESC')
      .limit(5)
      .pluck(:encestado_points)
      .reverse
  end

  def team
    super || NullTeam.new
  end
  
  # Returns if the player is active this season
  def is_active?
  	roster_status == 1
  end

  def zero_or_stat(col)
    output = self[col].to_f
    output.nil? ? '0.0' : output
  end
  
  def self.search(search)
    if search
      where('lower(display_first_last) LIKE ?', "%#{search.downcase}%")
    else
      all
    end
  end

  def points_in_week(week)
    player_points = ef_points.where(week_id: week.id)
    player_points.empty? ? 0 : player_points.first.points
  end
end
