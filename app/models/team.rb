class Team < ActiveRecord::Base
  extend FriendlyId
  friendly_id :team_name, use: :slugged

  has_many :players

	has_attached_file :avatar, default_url: "/images/:style/missing.png"
  	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def full_team_name
  	"#{team_city} #{team_name}"
  end
  	
end
