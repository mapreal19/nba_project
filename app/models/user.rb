class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :login

  validates_uniqueness_of :username
  validates_presence_of :username
  validates_exclusion_of :username,
                         in: %w( admin superuser ),
                         message: 'Borat says "Naughty, naughty!"'

  validates_length_of :username, within: 3..20

  #->Prelang (user_login:devise/username_login_support)
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login))
      where(conditions).where(['lower(username) = :value OR lower(email) = :value', { value: login.downcase }]).first
    else
      where(conditions).first
    end
  end

  devise authentication_keys: [:login]

  has_many :players, through: :user_players
  has_many :user_players
  has_many :user_points
  has_many :user_lineups

  scope :user_ranking, -> do
    joins(:user_points)
      .select('users.id, users.username, sum(points) as points, 0 as difference')
      .group('users.id')
      .order('points desc')
  end

  scope :ranking_in_week, ->(week) do
    find_by_sql(
      "
      SELECT u.id, u.username, up.points, (up.points - up2.points) as difference
      FROM users u
      LEFT JOIN user_points up ON up.user_id = u.id
      LEFT JOIN user_points up2 ON up2.user_id = u.id
      WHERE up.week_id = #{week.id}
      AND up2.week_id = #{week.previous_week.id}
      ORDER BY up.points DESC;
      ")
  end

  def self.all_users_and_total_points
    user_ranking
  end

  def self.all_users_and_points_in_week(week)
    previous_week = week.previous_week
    if previous_week
      ranking_in_week(week)
    else
      user_ranking.where('user_points.week_id = ?', week.id)
    end
  end

  def all_points
    user_points.sum(:points)
  end

  def budget_spent
    players.sum(:salary)
  end

  def budget_left
    UserPlayer::BUDGET - budget_spent
  end

  def team_complete?
    UserPlayer::MAX_NUM_PLAYERS == user_players.count
  end

  def player_in_team?(player)
    UserPlayer.exists?(player_id: player.id, user_id: id)
  end

  def points_in_week(week)
    usr_points = user_points.where(week_id: week.id)
    usr_points.empty? ? 0 : usr_points.first.points
  end

end
