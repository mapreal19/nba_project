class UserPlayer < ActiveRecord::Base
  MAX_NUM_PLAYERS = 8
  BUDGET = 65000000

  belongs_to :user
  belongs_to :player
  validates_with MaxNumTeamPlayersValidator
  validates_with MaxBudgetReachedValidator
  
  # Para evitar tener dos veces el mismo jugador
  validates_uniqueness_of :player_id, scope: :user_id
  
  scope :salary_desc, -> { includes(:player).order('players.salary DESC') }

end