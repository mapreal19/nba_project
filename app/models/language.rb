class Language < ActiveRecord::Base
  def is_current?
    locale == I18n.locale.to_s
  end
end
