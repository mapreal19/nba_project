json.array!(@players) do |player|
  json.extract! player, :id, :name, :display_first_last
  json.url player_url(player)
end
