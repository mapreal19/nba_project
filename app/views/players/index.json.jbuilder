json.array!(@players) do |player|
  json.extract! player, :id, :name, :roster_status, :from_year, :to_year, :player_code, :salary, :avatar, :team, :slug, :display_first_last
  json.url player_url(player)
end
