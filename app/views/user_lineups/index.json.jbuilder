json.array!(@user_lineups) do |user_lineup|
  json.extract! user_lineup, :id, :index, :show
  json.url user_lineup_url(user_lineup, format: :json)
end
