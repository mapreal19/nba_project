require 'open-uri'

class NbaTeamInfo
  
  def self.get_all_teams
    # Team ID go from to 
    (37..66).each do |team_id|
      # TeamID = 16106127 + team_id
      result = JSON.parse(
        open("http://stats.nba.com/stats/teaminfocommon?Season=2013-14&SeasonType=Regular+Season&LeagueID=00&TeamID=16106127#{team_id}")
        .read
        )
      
      team_info = result["resultSets"][0]["rowSet"][0]
      team_season_rank = result["resultSets"][1]["rowSet"][0]

      unless Team.exists?(id: team_info[0])
        Team.create(
          id: team_info[0],
          team_id: team_info[0],
          season_year: team_info[1],
          team_city: team_info[2],
          team_name: team_info[3],
          team_abbreviation: team_info[4],
          team_conference: team_info[5],
          team_division: team_info[6],
          team_code: team_info[7],
          wins: team_info[8],
          losses: team_info[9],
          pct: team_info[10],
          conf_rank: team_info[11],
          div_rank: team_info[12]
          )
      end

      # !TODO
      # Save TeamSeasonRank to the database

    end # [END] each loop
  end # [END] get_all_teams

  def self.get_team_images
    Team.all.each do |team|
      # http://stackoverflow.com/questions/4049709/save-image-from-url-by-paperclip
      team.avatar = URI.parse("http://stats.nba.com/media/logos/#{team.team_abbreviation}_96x96.png")
      team.save
    end
  end

end