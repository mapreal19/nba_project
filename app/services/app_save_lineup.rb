class AppSaveLineup
  
  def self.run(week_obj)
    User.all.each do |user|
      users_lineups = []
      user.user_players.each do |user_player|
		users_lineups << { week_id: week_obj.id, user_id: user.id, player_id: user_player.player_id }
      end

      UserLineup.create!(users_lineups)
    end
  end

end