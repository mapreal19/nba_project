require 'open-uri'

class NbaPlayerExtraInfo
	
	def self.run
		# http://stats.nba.com/stats/commonplayerinfo/?PlayerID=1717&SeasonType=Regular+Season
		Player.all.each do |player|
			query =  JSON.parse(
				open("http://stats.nba.com/stats/commonplayerinfo/?PlayerID=#{player.id}&SeasonType=Regular+Season")
				.read
				)

			update_player_info(query, player)
			update_player_stats(query, player)
		end
	end

	private

	def self.update_player_info(query, player)
		player_info = query["resultSets"][0]["rowSet"][0]

		player.update(
      display_first_last: player_info[3],
			birthdate: player_info[6],
			school: player_info[7],
			country: player_info[8],
			last_affiliation: player_info[9],
			jersey: player_info[10],
			position: player_info[11],
			team_id: player_info[13]
			)

	end

	def self.update_player_stats(query, player)
		player_stats = query["resultSets"][1]["rowSet"][0]
		
		unless player_stats.nil?
			player.update(
				pts: player_stats[3],
				ast: player_stats[4],
				reb: player_stats[5]
				)
		end

	end

end