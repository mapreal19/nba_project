class AppComputePoints
  def self.run(week_obj)
    update_ef_points(week_obj)
    User.all.each do |user|
      sum = 0
      user.user_lineups.where(week: week_obj).each do |user_player|
        ef_points = user_player.player.ef_points.where(week: week_obj)
        sum += ef_points.first.points unless ef_points.first.nil?
      end
      UserPoint.create(week_id: week_obj.id, user_id: user.id, points: sum)
    end
  end
  
  private
    
  def self.update_ef_points(week_obj)
    player_averages = Player.average_encestado_points_between(week_obj.start_date, week_obj.end_date)
    ef_points = []
    player_averages.each do |player|
      ef_points << { points: player.average, week_id: week_obj.id, player_id: player.id }
    end
    EfPoint.create!(ef_points)
  end
end