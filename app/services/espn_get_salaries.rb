require 'open-uri'

class EspnGetSalaries

  def self.run
    a = Mechanize.new { |agent|
      agent.user_agent_alias = 'Mac Safari'
    }
    
    page = a.get('http://espn.go.com/nba/salaries')
    get_salaries_from(page)

    # Espn has 15 pages
    (2..15).each do |i|
      page = a.get("http://espn.go.com/nba/salaries/_/page/#{i}")
      get_salaries_from(page)
    end
  end
  
  private 
  
  def self.get_salaries_from(page)
    page.search('//tr[@class="evenrow" or @class="oddrow"]').each do |player|
      name_player = player.search("td[2] a").children.text
      matched_data = /(?<first_name>\w*)\s(?<last_name>\w*)/.match(name_player)
      last_name_comma_first_name = matched_data[:last_name] + ', ' + matched_data[:first_name]

      # salary in form $321,234,323
      salary = player.search('td[4]').text.tr('$,', '')

      player_in_db = Player.find_by(name: last_name_comma_first_name) #TODO: add index in DB
      unless player_in_db.nil?
        player_in_db.update(salary: salary)
      end
    end
  end

  
  
end