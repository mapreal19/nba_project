class NbaGameLogs < Nba
  
  def self.run
    game_logs = []
    Player.find_each do |player|
      game_logs += game_logs_of_player(player)
    end
    GameLog.create!(game_logs)
  end
  
  def self.game_logs_of_player(player)
    result = json_from_url("#{BASE_URL}playergamelog?Season=2014-15&SeasonType=Regular+Season&PlayerID=#{player.id}")
    keys = result['resultSets'][0]['headers'].map(&:downcase)
    game_logs = result['resultSets'][0]['rowSet']
    
    # We need only the game logs that don't exist
    game_logs.reject! do |game_log|
      GameLog.exists?(player_id: player.id, game_id: game_log[2])
    end
    game_logs.each_with_object([]) do |game_log, a|
      a << game_log_record(keys, game_log)
    end
  end
  
  def self.game_log_record(keys, game_log)
    record = Hash[keys.zip game_log]
    record[:encestado_points] = compute_points(record)
    record
  end
  
  def self.compute_points(game_log)
    # pts + reb + ast + stl + blk - tov - (fta - ftm) - (fga - fgm)
    keys_to_sum = ['pts', 'reb', 'ast', 'stl', 'blk']
    negative_points = game_log['tov'] + (game_log['fta'] - game_log['ftm']) + (game_log['fga'] - game_log['fgm'])
    game_log.values_at(*keys_to_sum).inject(:+) - negative_points
  end
  
end
