require 'open-uri'

class Nba
  BASE_URL = 'http://stats.nba.com/stats/'
  
  def self.json_from_url(url)
    JSON.parse(open(url).read)
  end
end
