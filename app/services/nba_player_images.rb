require 'open-uri'

class NbaPlayerImages
  
  def self.run
    Player.all.each do |player|
      # http://stackoverflow.com/questions/4049709/save-image-from-url-by-paperclip
      
      url = URI.parse("http://stats.nba.com/media/players/230x185/#{player.id}.png")
      # http://stackoverflow.com/questions/5908017/check-if-url-exists-in-ruby
      req = Net::HTTP.new(url.host, url.port)
      res = req.request_head(url.path)
      
      if res.code == '200'
        player.avatar = url
        player.save
      end
      
    end
  end
  
end