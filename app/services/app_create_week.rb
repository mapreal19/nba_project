class AppCreateWeek
  def self.run(start_date, end_date = start_date + 6)
    week = Week.create!(start_date: start_date, end_date: end_date)
    AppSaveLineup.run(week)
    previous_week = Week.find_by(end_date: start_date - 1)
    AppComputePoints.run(previous_week) unless previous_week.nil?
  end
end
