require 'open-uri'

class NbaPlayers
  
  def self.run
    Player.delete_all    

    result = JSON.parse(
      open("http://stats.nba.com/stats/commonallplayers/?LeagueID=00&Season=2014-15&IsOnlyCurrentSeason=1")
      .read
      )
    players = result["resultSets"][0]["rowSet"]

    players.each do |player|
      unless Player.exists?(id: player[0])
        Player.create(
          id: player[0],
          name: player[1],
          roster_status: player[2],
          from_year: player[3],
          to_year: player[4],
          player_code: player[5]
          )
      end 
    end 
   
    delete_players_from_lineups     
  end
  
  private 
  
  def self.delete_players_from_lineups
    UserPlayer.all.each do |user_player|
      unless Player.exists?(id: user_player.player_id)
        user_player.delete
      end
    end
  end
 
end