require 'open-uri'

class HoopshypeGetSalaries

  def self.run
    a = Mechanize.new { |agent| agent.user_agent_alias = 'Mac Safari' }
    
    page = a.get('http://hoopshype.com/salaries.htm')
    
    get_teams_from_table(a, page, 1)
    get_teams_from_table(a, page, 2)    
  end
  
  private
  
  def self.get_teams_from_table(agent, page, table_id)
    (3..7).each do |i|
      page.search("//*[@id='content']/div[1]/div[2]/div[#{table_id}]/table/tbody/tr[#{i}]").each do |row|
        row.css("a").each do |link|
          new_page = link['href']
          team_page = agent.get("http://hoopshype.com#{new_page}")

          get_salaries_from_team_page(team_page)
        end
      end
    end      
  end
  
  def self.get_salaries_from_team_page(page)
    page.search('#content > div.p402_premium table tr').each_with_index do |player_row, index|
      if index != 0 and index != page.search('#content > div.p402_premium table tr').size - 1
        name_player = player_row.css("td")[0].text
        matched_data = /(?<first_name>\w*)\s(?<last_name>\w*)/.match(name_player)
        unless matched_data.nil?
          last_name_comma_first_name = matched_data[:last_name] + ', ' + matched_data[:first_name]
          salary = player_row.css("td")[1].css("span").text.tr('$,', '')  
          
          update_player_salary(last_name_comma_first_name, salary)
          
        end
      end
    end
  end
  
  def self.update_player_salary(name, salary)
    player_in_db = Player.find_by(name: name) 
      unless player_in_db.nil?
        player_in_db.update(salary: salary)
      end
  end
  
end