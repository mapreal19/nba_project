class MaxNumTeamPlayersValidator < ActiveModel::Validator
  def validate(user_player)
    if user_player.user.user_players.size > UserPlayer::MAX_NUM_PLAYERS - 1
      user_player.errors[:base] << It.it('teams.max_num')
    end
  end
end