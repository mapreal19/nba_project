class MaxBudgetReachedValidator < ActiveModel::Validator
  def validate(user_player)
    unless user_player.player.salary.nil?
      if user_player.user.budget_spent + user_player.player.salary > UserPlayer::BUDGET
        user_player.errors[:base] << 'No se puede superar el presupuesto'
      end
    end
  end
end