class PlayersController < ApplicationController
  before_action :set_player, only: :show

  # GET /players
  # GET /players.json
  def index
    respond_to do |format|
      format.html 
      format.json { 
        @players = Player.salary_desc.includes(:team)
      }
    end
  end

  # GET /players/1
  # GET /players/1.json
  def show
    @ef_points = @player.ef_points.includes(:week)
    @game_logs = @player.game_logs.order(:game_date)
  end
  
  def search
    @players = Player.search(params[:q]).includes(:team)
    render 'players/search.json.jbuilder'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player
      @player = Player.friendly.find(params[:id])
    end
  end
