class TeamsController < ApplicationController

	def index
		@teams = Team.all
	end
  
  def show
    @team = Team.friendly.find(params[:id])
    @team_players = @team.players.salary_desc
  end
  


end