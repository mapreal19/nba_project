class Users::PasswordsController < Devise::PasswordsController
  include ApplicationHelper

  def new
    super
  end

  # http://bloginius.com/blog/2014/08/01/another-guide-to-upgrade-to-rails-4-dot-1/
  def create
    @user = resource_class.send_reset_password_instructions(resource_params.permit(:email))

    redirect_to :root, flash: { notice: t('email.reset_confirm') }
  end


end
