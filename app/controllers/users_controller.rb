class UsersController < ApplicationController
  # GET /users
  # GET /users.json
  def index
    @users = User.all_users_and_total_points
  end

  def show
    @week = Week.find(params[:id])
    @users = User.all_users_and_points_in_week(@week)
  end
end
