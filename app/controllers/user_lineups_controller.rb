class UserLineupsController < ApplicationController
  before_action :require_user_signed_in

  # GET /user_lineups
  # GET /user_lineups.json
  def index
    #@user_lineups = UserLineup.all
    # @weeks = current_user.user_lineups.week.uniq!
    @weeks = Week.all
  end

  # GET /user_lineups/1
  # GET /user_lineups/1.json
  def show
    @user_players = current_user.user_lineups.where(week_id: params[:id])
    @week = Week.find(params[:id])
  end

end
