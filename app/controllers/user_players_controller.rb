class UserPlayersController < ApplicationController
  before_action :set_user_player, only: :destroy
  before_action :set_players_available, only: :new
  before_action :require_user_signed_in

  # GET /user_players
  # GET /user_players.json
  def index
    @user_players = current_user.user_players.salary_desc
  end

  # GET /user_players/new
  def new
    @user_player = UserPlayer.new
  end

  # POST /user_players
  # POST /user_players.json
  def create
    @user_player = UserPlayer.new(user_player_params)
    @user_player.user = current_user

    respond_to do |format|
      if @user_player.save
        format.html { redirect_to user_players_path, notice: It.it("players.added", user: @user_player.player.name) }
        format.json { render action: 'show', status: :created, location: @user_player }
      else
        format.html do  
          set_players_available
          render action: 'new' 
        end
        
        format.json { render json: @user_player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_players/1
  # DELETE /user_players/1.json
  def destroy
    @user_player.destroy
    respond_to do |format|
      format.html { redirect_to user_players_url, notice: 'Jugador traspasado.' }
      format.json { head :no_content }
      format.js { render nothing: true }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_player
      @user_player = UserPlayer.find(params[:id])
    end
  
  def set_players_available
    @players_available = Player.with_salary.includes(:team) - current_user.players
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_player_params
      params.require(:user_player).permit(:player_id)
    end
end
