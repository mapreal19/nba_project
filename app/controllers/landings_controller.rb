class LandingsController < ApplicationController
  layout 'landing'

  def index
		redirect_to user_players_url if user_signed_in?    
  end
end
