$ ->

  responsiveHelper = undefined
  breakpointDefinition =
    tablet: 1024
    phone: 480

  tableElement = $(".dataTable")
  tableElement.dataTable
    pagingType: 'simple'
    autoWidth: false
    dom: 'frtp'
    order: [[2, 'desc']]
    
    language:
      search: 'Buscar:'
      zeroRecords: 'No se encontró ningún jugador...'
      paginate:
        previous: 'Anterior'
        next: 'Siguiente'
    
    
    preDrawCallback: ->

      # Initialize the responsive datatables helper once.
      responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition)  unless responsiveHelper
      return

    rowCallback: (nRow) ->
      responsiveHelper.createExpandIcon nRow
      return

    drawCallback: (oSettings) ->
      responsiveHelper.respond()
      return