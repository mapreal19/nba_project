$( document ).ready(function() {
  
  jQuery('.search').click(function () {
    if(jQuery('.search-btn').hasClass('glyphicon-search')){
      jQuery('.search-open').fadeIn(500);
      jQuery('.search-btn').removeClass('glyphicon-search');
      jQuery('.search-btn').addClass('glyphicon-remove');
    } else {
      jQuery('.search-open').fadeOut(500);
      jQuery('.search-btn').addClass('glyphicon-search');
      jQuery('.search-btn').removeClass('glyphicon-remove');
    }   
  }); 
  
  
  // instantiate the bloodhound suggestion engine
  var players = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('display_first_last'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: window.location.protocol + "//" + window.location.host + '/players/search/%QUERY'
    }
  });

  // initialize the bloodhound suggestion engine
  players.initialize();

  // instantiate the typeahead UI
  $('#search .typeahead').typeahead(
    {
      highlight: true,
      minLength: 1, // send AJAX request only after user type in at least 3 characters
      hint: true
    }, 
    {
      displayKey: 'display_first_last',
      source: players.ttAdapter(),
      templates: {
        empty: [
        '<div class="empty-message">',
        'No se encontró ningún jugador',
        '</div>'
        ].join('\n'),
        suggestion: function(data){
          return '<a href="' + data.url + '">'+ data.display_first_last + '</a>';
        }
    }
  });
});

