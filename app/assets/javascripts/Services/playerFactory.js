angular.module('encestadoApp').factory('playerFactory', ['$http', function($http){
	return {
		all: function (){
			return $http.get('/players.json');
		},
		find: function (id) {
			return $http.get('/players/' + id + '.json');
		}
	}
}])