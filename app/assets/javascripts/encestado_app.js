angular.module('encestadoApp', ['ui.bootstrap']);

// This will cause your app to compile when Turbolinks loads a new page
// and removes the need for ng-app in the DOM
// http://kurtfunai.com/2014/08/angularjs-and-turbolinks.html
$(document).on('ready page:load', function(arguments) {
	angular.bootstrap(document.body, ['encestadoApp']);
});


// Without this you cannot make valid requests to your Rails application.
angular.module('encestadoApp', ['ui.bootstrap'])
.config(["$httpProvider", function(provider) {
	provider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
}]);