// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require angular
//= require angular-ui-bootstrap-tpls
//= require turbolinks
//= require bootstrap
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require dataTables/extras/dataTables.responsive
//= require nprogress
//= require nprogress-turbolinks
//= require fittext
//= require twitter/typeahead

//= require ./encestado_app
//= require_tree .

$(document).on('ready page:load', function () {
	
	$('ul.pagination').on('click','li',function(){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

});