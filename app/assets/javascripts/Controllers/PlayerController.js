angular.module('encestadoApp').controller('PlayerController',['$scope', 'playerFactory', function ($scope, playerFactory) {
	
	$scope.players = [];

	playerFactory.all().success(function (players) {
		$scope.players = players;
	});

	$scope.currentPage = 1; //current page
    $scope.maxSize = 5; //pagination max size
    $scope.entryLimit = 12; //max rows for data table

    /* init pagination with $scope.list */
    $scope.noOfPages = Math.ceil($scope.players.length/$scope.entryLimit);
    
    $scope.filter = function() {
        $timeout(function() { 
            //wait for 'filtered' to be changed
            /* change pagination with $scope.filtered */
            $scope.currentPage = 1;
            $scope.noOfPages = Math.ceil($scope.filtered.length/$scope.entryLimit);
        }, 10);
    };

}]);

angular.module('encestadoApp').filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
