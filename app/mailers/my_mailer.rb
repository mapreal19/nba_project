class MyMailer < Devise::Mailer
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  
  default from: 'Encestado <no-reply@encestado.com>'

  def reset_password_instructions(record, token, opts={})
    #opts[:from] = 'no-reply@encestado.com'
    #opts[:reply_to] = 'no-reply@encestado.com'
    super
  end
  
end