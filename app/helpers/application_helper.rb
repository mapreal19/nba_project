module ApplicationHelper
  def first_time_visit?
    cond = cookies[:first_time].nil?
    cookies.permanent[:first_time] = 1
    cond
  end

  def language_options
    Language.all.each_with_object('') do |language, options|
      option = "<option "
      option += "selected " if language.is_current?
      option += "value='#{url_for(locale: language.locale)}'>#{language.name}</option>"
      options << option
    end.html_safe
  end
end
