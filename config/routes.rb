NbaProject::Application.routes.draw do
  scope '(:locale)', locale: /#{I18n.available_locales.join('|')}/ do
    devise_for :users, controllers: {registrations: "users/registrations", sessions: "users/sessions", passwords: "users/passwords"}, skip: [:sessions, :registrations]

    resources :players, only: [:index, :show] do
      collection do 
        get 'search/:q', :action => 'search', :as => 'search'
      end
    end
   
    resources :user_players, only: [:index, :new, :create, :destroy]
    resources :users, only: [:index, :show]
    resources :user_lineups, only: [:index, :show]
    
    resources :teams, only: [:index, :show]

    root 'landings#index'

    #->Prelang (user_login:devise/stylized_paths)
    devise_scope :user do
      get    "login"   => "devise/sessions#new",         as: :new_user_session
      post   "login"   => "devise/sessions#create",      as: :user_session
      delete "signout" => "devise/sessions#destroy",     as: :destroy_user_session
      
      get    "signup"  => "devise/registrations#new",    as: :new_user_registration
      post   "signup"  => "devise/registrations#create", as: :user_registration
      put    "signup"  => "devise/registrations#update", as: :update_user_registration
      delete "signup"  => "users/registrations#destroy", as: :destroy_user
      get    "account" => "devise/registrations#edit",   as: :edit_user_registration
    end

    if Rails.env.development?
      mount LetterOpenerWeb::Engine, at: "/letter_opener"
    end
  end
end
