require 'rails_helper'

feature 'User signs up' do
  def sign_up_with(email, password)
    visit new_user_registration_path

    fill_in 'Usuario', with: 'Mario'
    fill_in 'Email', with: email
    fill_in 'Contraseña', with: password
    fill_in 'Repite Contraseña', with: password

    click_button 'Enviar'
  end

  scenario 'with valid email and password' do
    sign_up_with 'valid@example.com', 'password'

    expect(page).to have_content('Cuenta')
  end

  scenario 'with invalid email' do
    sign_up_with 'invalid_email', 'password'

    expect(page).to have_content('Registro')
  end

  scenario 'with blank password' do
    sign_up_with 'valid@example.com', ''

    expect(page).to have_content('Registro')
  end
end
