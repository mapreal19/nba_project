VCR.configure do |c|
  c.cassette_library_dir = Rails.root.join("spec", "vcr")
  c.hook_into :webmock
  # https://www.relishapp.com/vcr/vcr/v/2-4-0/docs/test-frameworks/usage-with-rspec-metadata
  c.configure_rspec_metadata!
  c.default_cassette_options = {
    re_record_interval: 1.week
  }
end