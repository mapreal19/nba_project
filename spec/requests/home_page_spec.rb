require 'rails_helper'

describe 'Home Page' do 

  it 'renders the page correctly with status 200' do
    get root_path
    expect(response.code).to be == '200'
  end

  it 'renders remember password page' do
    get new_user_password_path
    expect(response.code).to be == '200'
  end
end