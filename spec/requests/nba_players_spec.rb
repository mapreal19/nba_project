require 'rails_helper'

describe 'Nba Players' do 
  
  let!(:fake_player) do 
    create(:player, id: 99999999)
  end
  
  let!(:nico) do 
    create(:user)
  end

  it 'gets all the players correctly', :vcr do
    NbaPlayers.run
    
    expect(Player.all.count).to be > 0
    
    # DIRK!! :D
    expect(Player.where(id: 1717)).to exist    
  end
  
  it 'deletes inactive players from the db', :vcr do 
    nico.user_players << UserPlayer.new(player_id: fake_player.id)
    expect(nico.user_players.count).to be == 1
    
    NbaPlayers.run
    
    expect(nico.user_players.count).to be == 0
  end

end