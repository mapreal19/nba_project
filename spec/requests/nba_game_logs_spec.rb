require 'rails_helper'

describe 'Nba Game Logs' do 

  it 'gets the logs correctly of player 1717', :vcr do
    uri = 'http://stats.nba.com/stats/playergamelog?Season=2014-15&SeasonType=Regular+Season&PlayerID=1717'
 
    response = Net::HTTP.get_response(URI(uri))
    json_response = JSON.parse(response.body)
        
    expect(response.code).to be == '200'
    expect(json_response['parameters']['PlayerID']).to be == 1717
  end

end