require "rails_helper"

describe User do
  # https://www.relishapp.com/rspec/rspec-core/v/2-6/docs/helper-methods/let-and-let
  let!(:nico) do 
    create(:user)
  end

  it "should not exist 2 users with the same username" do
    user1 = create(:user, username: 'pablo')

    expect(build(:user, username: 'pablo', email: 'foo@mail.com')).not_to be_valid
  end

  it "username must not be blank" do
    expect(build(:user, username:'', email: 'foo@mail.com')).not_to be_valid
  end

  it "username must not have more than 20 characters" do
    usrname = 'a' * 21
    expect(build(:user, username: usrname)).not_to be_valid
  end
  
  it "fails when adding more than 8 players to a user" do
    8.times do |i|
      player = create(:player)
      user_player = UserPlayer.create(user_id: nico.id, player_id: player.id) 
    end
    
    player = create(:player)
    expect(UserPlayer.new(user_id: nico.id, player_id: player.id)).not_to be_valid
  end
  
  it "fails when trying to add a player that exceeds the budget" do 
    player1 = create(:player, salary: 40000000)
    player2 = create(:player, salary: 30000000)
    
    user_player = UserPlayer.new(user_id: nico.id, player_id: player1.id)
    expect(user_player).to be_valid
    user_player.save!
    
    expect(UserPlayer.new(user_id: nico.id, player_id: player2.id)).not_to be_valid   
  end
  
  it "budget spent should return 0 if no players in the team" do 
    expect(nico.budget_spent).to eq(0)
  end


  it "team complete sould return true if max players reached" do
    expect(nico.team_complete?).to be_falsey

    UserPlayer::MAX_NUM_PLAYERS.times do |i|
      player = create(:player)
      user_player = UserPlayer.create(user_id: nico.id, player_id: player.id) 
    end
    expect(nico.team_complete?).to be_truthy
  end

  it "calculates the difference in points between two weeks correctly" do
    first_week = Week.create
    second_week = Week.create
    nico.user_points << UserPoint.create(points: 10, week: first_week)
    nico.user_points << UserPoint.create(points: 15, week: second_week)

    expect(User.all_users_and_points_in_week(Week.last).first.difference).to eq 5
  end

  it "outputs the difference correctly when just one week" do
    week = Week.create
    nico.user_points << UserPoint.create(points: 10, week: week)

    expect(User.all_users_and_points_in_week(Week.last).first.difference).to eq 0
  end

end