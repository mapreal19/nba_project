require "rails_helper"

describe Player do
  # https://www.relishapp.com/rspec/rspec-core/v/2-6/docs/helper-methods/let-and-let
  let!(:kobe) do 
    create(:player)
  end


  it "should output the stats correctly" do
    kobe.pts = 23.4
    kobe.reb = 5.6
    kobe.ast = 4.5

    expect(kobe.zero_or_stat(:pts)).to be == 23.4
    expect(kobe.zero_or_stat(:reb)).to be == 5.6
    expect(kobe.zero_or_stat(:ast)).to be == 4.5
  end
  
  it "should output 0.0 when the player has no stats" do
    expect(kobe.zero_or_stat(:pts)).to be == 0
    expect(kobe.zero_or_stat(:reb)).to be == 0
    expect(kobe.zero_or_stat(:ast)).to be == 0
  end
end