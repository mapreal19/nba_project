FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "foo-#{n}" }
    password "foobar12"
    password_confirmation "foobar12"
    email {"#{username}@gmail.com"}
  end
  
  factory :player do
    sequence(:name) { |n| "Jordan-#{n} " }
  end
  
  # 36 Efpoints
  factory :game_log do
    fgm 5
    fga 8
    ftm 7
    fta 8
    pts 20
    reb 8
    ast 13
    blk 0
    stl 2
    tov 3
    encestado_points 36
  end
  
  factory :game_log_from_nba, class:Hash do 
    initialize_with { 
      { "season_id"=>"22014", "player_id"=>1717, "game_id"=>"0021400094", "game_date"=>"NOV 09, 2014", "matchup"=>"DAL vs. MIA", "wl"=>"L", "min"=>30, "fgm"=>6, "fga"=>15, "fg_p
ct"=>0.4, "fg3m"=>2, "fg3a"=>3, "fg3_pct"=>0.667, "ftm"=>3, "fta"=>4, "ft_pct"=>0.75, "oreb"=>0, "dreb"=>6, "reb"=>6, "ast"=>3, "stl"=>1, "blk"=>1, "tov"=>1, "pf"=>0, "pts"=>
17, "plus_minus"=>-11, "video_available"=>1 } 
    }
  end
  
end