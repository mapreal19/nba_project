require 'rails_helper'

describe 'App Compute points' do 
  let(:week_date) { Date.parse('25/09/2014') }
  let(:week_end_date) { week_date + 6 }
  
  let(:week_obj) { Week.create(start_date: week_date, end_date: week_end_date) }
  
  let(:player1) { create(:player) }
  let(:player2) { create(:player) }
  
  let!(:gamelog1) do 
    create(:game_log, player_id: player1.id, game_date: week_date + 4)
  end
  let!(:gamelog2) do 
    create(:game_log, player_id: player2.id, game_date: week_date + 3)
  end
  # This game log should not be included
  let!(:gamelog3) do 
    create(:game_log, pts: 4, player_id: player1.id, game_date: week_date - 1, encestado_points: 20)
  end
  
  let!(:user) { create(:user) }

  it 'compute the points correctly' do
    user.user_players << UserPlayer.new(player_id: player1.id)
    user.user_players << UserPlayer.new(player_id: player2.id)
    AppSaveLineup.run(week_obj)
    AppComputePoints.run(week_obj)    
    expect(EfPoint.first.points).to eq(36)  
    expect(user.user_points.where(week: week_obj).first.points).to eq(72)
  end
  
  it 'should compute the points without including the games played 1 week after' do 
    user.user_players << UserPlayer.new(player_id: player1.id)
    user.user_players << UserPlayer.new(player_id: player2.id)
    AppSaveLineup.run(week_obj)
    # Ef Points = 26. Avg player 1 = 31
    create(:game_log, pts: 10, player_id: player2.id, game_date: week_end_date, encestado_points: 26)
    # This game won't be included
    create(:game_log, pts: 13, player_id: player2.id, game_date: week_end_date + 1)
    AppComputePoints.run(week_obj)
    expect(user.user_points.where(week: week_obj).first.points).to eq(67)
  end
  
  it 'should compute 0 points for a player with no games played' do 
    player = create(:player)
    AppComputePoints.run(week_obj)
    expect(EfPoint.where(player_id: player.id, week_id: week_obj.id).first.points).to eq(0)
  end

end
