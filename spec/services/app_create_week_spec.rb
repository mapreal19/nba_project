require 'rails_helper'

describe 'App Create Week' do
  let(:week_date) { Date.parse('25/09/2014') }
  
  let(:week_obj) { Week.create(start_date: week_date, end_date: week_date + 1.week) }
  
  let(:player1) { create(:player) }
  let(:player2) { create(:player) }
  
  let!(:gamelog1) do
    create(:game_log, player_id: player1.id, game_date: week_date - 4)
  end
  let!(:gamelog2) do
    create(:game_log, player_id: player2.id, game_date: week_date - 3)
  end

  let!(:user) { create(:user) }

  it 'should compute the points correctly twice' do 
    user.user_players << UserPlayer.new(player_id: player1.id)
    user.user_players << UserPlayer.new(player_id: player2.id)
    end_date = week_date + 6

    # First week (we just save the lineup)
    AppCreateWeek.run(week_date - 1.week)
    
    # Week 2 (Calculate points between week 1 and 2)
    # Week -> 18/09/2014 - 24/09/2014
    AppCreateWeek.run(week_date, end_date)
    
    create(:game_log, pts: 10, player_id: player1.id, game_date: week_date + 3, encestado_points: 26)
    expect(player1.ef_points.count).to eq(1)
    
    expect(user.user_points.where(week_id: Week.first).first.points).to eq(72)

    # Week 3 (Calculate points between week 2 and 3)
    AppCreateWeek.run(end_date + 1)
    expect(player1.ef_points.count).to eq(2)
    expect(user.user_points.where(week_id: Week.second).first.points).to eq(26)
    expect(user.all_points).to eq(98)
  end

end
