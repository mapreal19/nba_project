require 'rails_helper'

describe 'NBA Game Logs' do 
  let!(:player1) do 
    create(:player, id: 1717) # Dirk from Dallas
  end
  
  it 'should get the game logs of a player correctly', :vcr do
    game_logs = NbaGameLogs.game_logs_of_player(player1)  
    # He scored 18 pts in the first game :D
    expect(game_logs.last['pts']).to eq 18
  end
  
  it 'should compute the efficiency points right from the game log' do
    a_game_log = build :game_log_from_nba
    expect(NbaGameLogs.compute_points(a_game_log)).to eq 17 # He did 17 in that game
  end

end
