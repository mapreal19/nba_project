require 'rails_helper'

describe 'App Save Lineup' do 

  it 'saves the lineup correctly' do
    date = Date.parse('12/10/2014')
    week = Week.create!(start_date: date, end_date: date + 1.week)
    
    user = create(:user)
    
    8.times do 
      player = create(:player)
      user.user_players << UserPlayer.new(player_id: player.id)
    end
    
    AppSaveLineup.run(week)
        
    expect(UserLineup.where(week_id: week.id, user_id: user.id).count).to eq(8)
  end

end