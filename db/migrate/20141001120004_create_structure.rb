class CreateStructure < ActiveRecord::Migration
  def change
    create_table "ef_points", force: :cascade  do |t|
      t.float    "points"
      t.integer  "player_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "week_id"
    end

    add_index "ef_points", ["player_id"], name: "index_ef_points_on_player_id"
    add_index "ef_points", ["week_id"], name: "index_ef_points_on_week_id"

    create_table "friendly_id_slugs", force: :cascade  do |t|
      t.string   "slug",                      null: false
      t.integer  "sluggable_id",              null: false
      t.string   "sluggable_type", limit: 50
      t.string   "scope"
      t.datetime "created_at"
    end

    add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

    create_table "game_logs", force: :cascade  do |t|
      t.date     "game_date"
      t.string   "matchup"
      t.string   "wl"
      t.integer  "min"
      t.integer  "fgm"
      t.integer  "fga"
      t.decimal  "fg_pct"
      t.integer  "fg3m"
      t.integer  "fg3a"
      t.decimal  "fg3_pct"
      t.integer  "ftm"
      t.integer  "fta"
      t.integer  "oreb"
      t.integer  "dreb"
      t.integer  "reb"
      t.integer  "ast"
      t.integer  "stl"
      t.integer  "blk"
      t.integer  "tov"
      t.integer  "pf"
      t.integer  "pts"
      t.integer  "player_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "game_id"
    end

    add_index "game_logs", ["player_id"], name: "index_game_logs_on_player_id"

    create_table "players", force: :cascade  do |t|
      t.string   "name"
      t.integer  "roster_status"
      t.integer  "from_year"
      t.integer  "to_year"
      t.string   "player_code"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.decimal  "salary",              default: 0.0
      t.string   "avatar_file_name"
      t.string   "avatar_content_type"
      t.integer  "avatar_file_size"
      t.datetime "avatar_updated_at"
      t.string   "birthdate"
      t.string   "school"
      t.string   "country"
      t.string   "last_affiliation"
      t.string   "jersey"
      t.string   "position"
      t.integer  "team_id"
      t.decimal  "pts"
      t.decimal  "reb"
      t.decimal  "ast"
      t.string   "display_first_last"
      t.string   "slug"
    end

    add_index "players", ["slug"], name: "index_players_on_slug", unique: true

    create_table "team_season_ranks", force: :cascade  do |t|
      t.string   "league_id"
      t.string   "seasion_id"
      t.integer  "team_id"
      t.integer  "pts_rank"
      t.float    "pts_pg"
      t.integer  "reb_rank"
      t.float    "reb_pg"
      t.integer  "ast_rank"
      t.float    "ast_pg"
      t.integer  "opp_pts_rank"
      t.float    "opp_pts_pg"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "teams", force: :cascade  do |t|
      t.string   "team_id"
      t.string   "season_year"
      t.string   "team_city"
      t.string   "team_name"
      t.string   "team_abbreviation"
      t.string   "team_conference"
      t.string   "team_division"
      t.string   "team_code"
      t.integer  "wins"
      t.integer  "losses"
      t.decimal  "pct"
      t.float    "conf_rank"
      t.float    "div_rank"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "avatar_file_name"
      t.string   "avatar_content_type"
      t.integer  "avatar_file_size"
      t.datetime "avatar_updated_at"
      t.string   "slug"
    end

    add_index "teams", ["slug"], name: "index_teams_on_slug", unique: true

    create_table "user_lineups", force: :cascade  do |t|
      t.integer  "user_id"
      t.integer  "player_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "week_id"
    end

    add_index "user_lineups", ["week_id"], name: "index_user_lineups_on_week_id"

    create_table "user_players", force: :cascade  do |t|
      t.integer  "user_id"
      t.integer  "player_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "user_players", ["player_id"], name: "index_user_players_on_player_id"
    add_index "user_players", ["user_id"], name: "index_user_players_on_user_id"

    create_table "user_points", force: :cascade  do |t|
      t.float    "points"
      t.integer  "user_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "week_id"
    end

    add_index "user_points", ["week_id"], name: "index_user_points_on_week_id"

    create_table "users", force: :cascade  do |t|
      t.string   "email",                  default: "", null: false
      t.string   "encrypted_password",     default: "", null: false
      t.string   "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",          default: 0,  null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.string   "current_sign_in_ip"
      t.string   "last_sign_in_ip"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "username"
    end

    add_index "users", ["email"], name: "index_users_on_email", unique: true
    add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    add_index "users", ["username"], name: "index_users_on_username", unique: true

    create_table "weeks", force: :cascade  do |t|
      t.date "start_date"
      t.date "end_date"
    end
    
    
  end
end
