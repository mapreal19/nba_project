class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :players, :team_id
    add_index :user_lineups, :user_id
    add_index :user_lineups, :player_id
  end
end
