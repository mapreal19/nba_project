class AddColumnsToGameLogs < ActiveRecord::Migration
  def change
    add_column :game_logs, :encestado_points, :integer
    add_column :game_logs, :season_id, :integer
    add_column :game_logs, :ft_pct, :decimal
    add_column :game_logs, :plus_minus, :integer
    add_column :game_logs, :video_available, :integer
  end
end
