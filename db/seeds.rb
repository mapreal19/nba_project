Language.delete_all
Language.create! [
                   {id: 1, name: 'Español', locale: :es},
                   {id: 2, name: 'English', locale: :en},
                 ]

# Regex for finding players with salary:
# ^.*salary: "\d{3,}.*$

# Finding: ^.*id: (\d*).*name: "(.*)", salary.*
# Replace: # Name: $2 \n Player.update($1, salary: "")

# # Name: Carter-Williams, Michael
# Player.update(203487, salary: "2300040")
# # Name: Nene
# Player.update(2403, salary: "13000000")
# # Name: Stoudemire, Amar'e
# Player.update(2405, salary: "23410988")
# # Name: Smith, J.R.
# Player.update(2747, salary: "5982375")
# # Name: Barea, Jose Juan
# Player.update(200826, salary: "4519500")
# # Name: Mayo, O.J.
# Player.update(201564, salary: "8000000")
# # Name: Hardaway Jr., Tim
# Player.update(203501, salary: "1250640")

=begin
# Name: Allen, Ray 
Player.update(951, salary: "")
# Name: O'Neal, Jermaine 
Player.update(979, salary: "")
# Name: Martin, Kenyon 
Player.update(2030, salary: "")
# Name: Turkoglu, Hedo 
Player.update(2045, salary: "")
# Name: Collins, Jason 
Player.update(2215, salary: "")
# Name: Butler, Caron 
Player.update(2406, salary: "")
# Name: Butler, Rasual 
Player.update(2446, salary: "")
# Name: Okafor, Emeka 
Player.update(2731, salary: "")
# Name: Udrih, Beno 
Player.update(2757, salary: "")
# Name: Price, Ronnie 
Player.update(101179, salary: "")
# Name: Hayes, Charles 
Player.update(101236, salary: "")
# Name: Lucas III, John 
Player.update(101249, salary: "")
# Name: Williams, Shawne 
Player.update(200761, salary: "")
# Name: Tucker, PJ 
Player.update(200782, salary: "")
# Name: Hollins, Ryan 
Player.update(200797, salary: "")
# Name: Sessions, Ramon 
Player.update(201196, salary: "")
# Name: Augustin, D.J. 
Player.update(201571, salary: "")
# Name: White, DJ 
Player.update(201591, salary: "")
# Name: Mbah a Moute, Luc 
Player.update(201601, salary: "")
# Name: Douglas-Roberts, Chris 
Player.update(201604, salary: "")
# Name: Jeffers, Othyus 
Player.update(201785, salary: "")
# Name: Casspi, Omri 
Player.update(201956, salary: "")
# Name: Mullens, Byron 
Player.update(201957, salary: "")
# Name: Cunningham, Dante 
Player.update(201967, salary: "")
# Name: Gee, Alonzo 
Player.update(202087, salary: "")
# Name: Turner, Evan 
Player.update(202323, salary: "")
# Name: Aminu, Al-Farouq 
Player.update(202329, salary: "")
# Name: Anderson, James 
Player.update(202341, salary: "")
# Name: Crawford, Jordan 
Player.update(202348, salary: "")
# Name: Smith, Ish 
Player.update(202397, salary: "")
# Name: Onuaku, Arinze 
Player.update(202620, salary: "")
# Name: Vesely, Jan 
Player.update(202686, salary: "")
# Name: Singleton, Chris 
Player.update(202698, salary: "")
# Name: Moore, E'Twaun 
Player.update(202734, salary: "")
# Name: Stone, Julyan 
Player.update(202933, salary: "")
# Name: Ayon, Gustavo 
Player.update(202970, salary: "")
# Name: Kidd-Gilchrist, Michael 
Player.update(203077, salary: "")
# Name: James, Bernard 
Player.update(203108, salary: "")
# Name: Lamb, Doron 
Player.update(203117, salary: "")
# Name: O'Quinn, Kyle 
Player.update(203124, salary: "")
# Name: Garrett, Diante 
Player.update(203197, salary: "")
# Name: Rice Jr., Glen 
Player.update(203318, salary: "")
# Name: Baynes, Aron 
Player.update(203382, salary: "")
# Name: Caldwell-Pope, Kentavious 
Player.update(203484, salary: "")
# Name: Siva, Peyton 
Player.update(203491, salary: "")
# Name: Ennis, James 
Player.update(203516, salary: "")
# Name: Thomas, Adonis 
Player.update(203519, salary: "")
# Name: Antetokounmpo, Thanasis 
Player.update(203648, salary: "")
# Name: McRae, Jordan 
Player.update(203895, salary: "")
# Name: Christon, Semaj 
Player.update(203902, salary: "")
# Name: Daniels, DeAndre 
Player.update(203908, salary: "")
# Name: McDaniels, KJ 
Player.update(203909, salary: "")
# Name: Grant, Jerami 
Player.update(203924, salary: "")
# Name: Brown, Alec 
Player.update(203938, salary: "")
# Name: Kirk, Alex 
Player.update(203945, salary: "")
# Name: O'Bryant, Johnny 
Player.update(203948, salary: "")
# Name: Stokes, Jarnell 
Player.update(203950, salary: "")
# Name: Cotton, Bryce 
Player.update(203955, salary: "")
# Name: Saric, Dario 
Player.update(203967, salary: "")
# Name: Bogdanovic, Bogdan 
Player.update(203992, salary: "")
# Name: Micic, Vasilije 
Player.update(203995, salary: "")
# Name: Jokic, NIkola 
Player.update(203999, salary: "")
# Name: Tavares, Walter 
Player.update(204002, salary: "")
# Name: Labeyrie, Louis 
Player.update(204010, salary: "")
=end