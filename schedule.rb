# Put this file in the server! Install the whenever gem!
# https://github.com/javan/whenever
#
# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

job_type :dokku_rails, "sudo -u dokku dokku run encestado.com rails r ':task'"
job_type :dokku_rake, "sudo -u dokku dokku run encestado.com rake :task"

every :thursday, :at => '11:00 am' do
  dokku_rake 'encestado:create_week'
end

every :day, :at => '10:00 am' do
  dokku_rake 'nba:game_logs'
end

# Clean up old (exited) containers
every :monday, :at => '9:00 am' do
  command "docker rm `docker ps -a | grep Exited | awk '{print $1 }'`"
end
